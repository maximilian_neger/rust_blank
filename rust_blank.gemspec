lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'rust_blank/version'

Gem::Specification.new do |spec|
  spec.name          = 'rust_blank'
  spec.version       = RustBlank::VERSION
  spec.authors       = ['Maximilian Neger']
  spec.email         = ['maximilian.neger@nix-wie-weg.de']
  spec.summary       = 'blank and friends implemented in rust'

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end

  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
  spec.extensions    = %w(rust/extconf.rb)

  spec.add_development_dependency 'bundler', '~> 1.11'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'activesupport', '~> 4.2.6'
  spec.add_development_dependency 'pry', '~> 0.10.3'
  spec.add_development_dependency 'pry-byebug'
  spec.add_development_dependency 'benchmark-ips', '~> 2.6.1'

  spec.add_runtime_dependency 'ffi', '1.9.10'
end
