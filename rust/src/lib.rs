extern crate libc;

use libc::{c_char, uint32_t};
use std::ffi::CStr;
use std::str;


fn validate<'a>(s: *const c_char) -> Option<&'a CStr> {
    unsafe {
        if !s.is_null() {
            Some(&CStr::from_ptr(s))
        } else {
            None
        }
    }
}

#[no_mangle]
pub extern "C" fn blank_rs(s: *const c_char) -> uint32_t {
    if let Some(c_str) = validate(s) {
        str::from_utf8(c_str.to_bytes())
            .map(|rust_str| {
                if rust_str.chars().all(|c| c.is_whitespace()) {
                    0
                } else {
                    1
                }
            })
            .unwrap_or(2)
    } else {
        2
    }
}
