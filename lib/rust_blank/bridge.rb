require 'ffi'

BUILD_TYPE = 'release'.freeze
EXT = if RUBY_PLATFORM.include?('darwin')
        'dylib'.freeze
      else
        'so'.freeze
      end

module RustBlank
  module Bridge
    extend FFI::Library

    ffi_lib "#{File.dirname(__FILE__)}/../../rust/target/#{BUILD_TYPE}/" \
      "librustblank.#{EXT}"

    attach_function :blank_rs, [:string], :int
  end
end
