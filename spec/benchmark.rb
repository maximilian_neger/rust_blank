$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)

require 'benchmark/ips'
require 'active_support'
require 'active_support/core_ext/object/blank'
require 'active_support/core_ext/string/multibyte'
require 'rust_blank'

FIXTURES = {
  ascii_8bit: File.read('spec/fixtures/ASCII-8BIT.out').lines.map do |line|
    line.encode('ASCII-8BIT')
  end,

  us_ascii: File.read('spec/fixtures/US-ASCII.out').lines.map do |line|
    line.encode('US-ASCII')
  end,

  utf_8: File.read('spec/fixtures/UTF-8.out').lines.freeze
}.freeze

def with_fallback(s)
  x = RustBlank::Bridge.blank_rs(s)
  if x == 1
    false
  elsif x == 0
    true
  else
    s.blank?
  end
end

Benchmark.ips do |x|
  x.iterations = 3
  x.config(time: 20, warmup: 3)

  x.report('active support blank?') do
    FIXTURES[:ascii_8bit].each(&:blank?)
    FIXTURES[:us_ascii].each(&:blank?)
    FIXTURES[:utf_8].each(&:blank?)
  end

  x.report('rust blank?') do
    FIXTURES[:utf_8].each { |s| RustBlank::Bridge.blank_rs(s) }
  end

  x.report('with fallback') do
    FIXTURES[:ascii_8bit].each { |s| with_fallback(s) }
    FIXTURES[:us_ascii].each { |s| with_fallback(s) }
    FIXTURES[:utf_8].each { |s| with_fallback(s) }
  end

  x.compare!
end
