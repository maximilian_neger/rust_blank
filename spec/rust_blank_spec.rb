# encoding: utf-8
require 'spec_helper'
require 'pry'

describe RustBlank::Bridge do
  it 'has a version number' do
    expect(RustBlank::VERSION).to eq '0.2.0'
  end

  describe do
    it 'works with utf-8' do
      expect(described_class.blank_rs(' x ')).to be 1

      ['', '   ', "  \n\t  \r ", '　', "\u00a0"].each do |value|
        expect(described_class.blank_rs(value)).to be 0
      end
    end

    it 'copes with ISO_8859_1' do
      expect(
        described_class.blank_rs(' x '.encode(Encoding::ISO_8859_1))
      ).to eq 1

      expect(
        described_class.blank_rs('  '.encode(Encoding::ISO_8859_1))
      ).to eq 0
    end

    it 'copes with utf-16' do
      expect(described_class.blank_rs(' x '.encode('utf-16'))).to eq 2
      expect(described_class.blank_rs('   '.encode('utf-16'))).to eq 2
    end

    # it 'copes with null bytes' do
    #   expect(described_class.blank_rs("\0".encode('binary'))).to eq 2
    # end
  end

  it 'works when called directly' do
    expect(RustBlank::Bridge.blank_rs(' ')).to be 0
  end
end
